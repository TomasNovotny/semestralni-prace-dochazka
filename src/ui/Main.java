package ui;

import app.Attendance;
import app.Employee;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.InputMismatchException;
import java.util.Scanner;
import utils.ExcelWriter;

/**
 *
 * @author Tomas
 */
public class Main {

    public static Scanner sc = new Scanner(System.in);
    public static DateTimeFormatter formatterDate = DateTimeFormatter.ofPattern("dd.MM.yyyy");
    public static DateTimeFormatter formatterTime = DateTimeFormatter.ofPattern("HH:mm");

    public static String redColor = "\033[31;1m";
    public static String resetColor = "\033[0m";

    public static void main(String[] args) throws IOException {
        boolean end = false;
        int choice = 0;
        Attendance at = new Attendance();
        try {
            at.Process("employees.dat", "shifts.txt");
        } catch (FileNotFoundException e) {
            System.out.println(ErrorWarning());
            System.out.println(redColor + "Soubor nenalezen! (" + e.getMessage() + ")");
            System.out.println(ErrorWarning());
        }

        while (!end) {
            boolean partEnd = false;
            boolean choiceBool = false;
            System.out.println("1 - zadat smenu");
            System.out.println("2 - prichod");
            System.out.println("3 - odchod");
            System.out.println("4 - zobrazit aktualne pracujici zamestnance");
            System.out.println("5 - smeny zamestnance");
            System.out.println("6 - exportovat smeny zamestnance");
            System.out.println("7 - seznam zamestnancu");
            System.out.println("8 - konec programu");
            while (!choiceBool) {
                try {
                    choice = sc.nextInt();
                    choiceBool = true;
                } catch (InputMismatchException e) {
                    System.out.println(ErrorWarning());
                    System.out.println("Musis zadat cislo! (" + e + ")");
                    System.out.println(ErrorWarning());
                    sc.next();
                }
            }
            switch (choice) {
                case 1:
                    while (!partEnd) {
                        try {
                            System.out.println("Zadej smenu ve formatu: ID DatumZacatku CasZacatku DatumKonce CasKonce (04.05.2019 18:00)");
                            System.out.println("<0 = konec zadavani smen");
                            try {
                                int employeeId = sc.nextInt();

                                if (employeeId > 0) {
                                    try {
                                        LocalDate StartDate = LocalDate.parse(sc.next(), formatterDate);
                                        LocalTime StartTime = LocalTime.parse(sc.next(), formatterTime);
                                        LocalDate EndDate = LocalDate.parse(sc.next(), formatterDate);
                                        LocalTime EndTime = LocalTime.parse(sc.next(), formatterTime);

                                        LocalDateTime StartDateTime = LocalDateTime.of(StartDate, StartTime);
                                        LocalDateTime EndDateTime = LocalDateTime.of(EndDate, EndTime);
                                        at.addShiftAll(StartDateTime, EndDateTime, employeeId);
                                    } catch (DateTimeParseException e) {
                                        System.out.println(ErrorWarning());
                                        System.out.println(redColor + "Zadej datum a cas ve spravnem formatu! (" + e.getMessage() + ")");
                                        System.out.println(ErrorWarning());
                                        System.out.println("");
                                    }

                                } else {
                                    partEnd = true;
                                }
                            } catch (InputMismatchException e) {
                                System.out.println(ErrorWarning());
                                System.out.println(redColor + "Musis zadat cislo! (" + e + ")");
                                System.out.println(ErrorWarning());
                                System.out.println("");
                                sc.next();
                            }
                        } catch (Exception e) {
                            System.out.println(ErrorWarning());
                            System.out.println(redColor + "Vyskytla se chyba, opakujte akci lepe! (" + e.getMessage() + ")");
                            System.out.println(ErrorWarning());
                            System.out.println("");
                            //sc.next(); 
                        }
                    }
                    break;
                case 2:
                    while (!partEnd) {
                        try {
                            System.out.println("Zadej prichod ve formatu: ID DatumPrichodu CasPrichodu (04.05.2019 18:00))");
                            System.out.println("<0 = konec zadavani prichodu");
                            try {
                                int employeeId = sc.nextInt();

                                if (employeeId > 0) {
                                    try {
                                        LocalDate StartDate = LocalDate.parse(sc.next(), formatterDate);
                                        LocalTime StartTime = LocalTime.parse(sc.next(), formatterTime);

                                        LocalDateTime StartDateTime = LocalDateTime.of(StartDate, StartTime);
                                        at.addShiftStart(StartDateTime, employeeId);
                                    } catch (DateTimeParseException e) {
                                        System.out.println(ErrorWarning());
                                        System.out.println(redColor + "Zadej datum a cas ve spravnem formatu! (" + e.getMessage() + ")");
                                        System.out.println(ErrorWarning());
                                        System.out.println("");
                                    }
                                } else {
                                    partEnd = true;
                                }
                            } catch (InputMismatchException e) {
                                System.out.println(ErrorWarning());
                                System.out.println(redColor + "Musis zadat cislo! (" + e + ")");
                                System.out.println(ErrorWarning());
                                System.out.println("");
                                sc.next();
                            }
                        } catch (Exception e) {
                            System.out.println(ErrorWarning());
                            System.out.println(redColor + e);
                            System.out.println(ErrorWarning());
                            System.out.println("");
                        }
                    }
                    break;

                case 3:
                    while (!partEnd) {
                        try {
                            System.out.println("Zadej odchod ve formatu: ID DatumOdchodu CasOdchodu (04.05.2019 18:00))");
                            System.out.println("<0 = konec zadavani odchodu");
                            try {
                                int employeeId = sc.nextInt();

                                if (employeeId > 0) {
                                    LocalDate EndDate = LocalDate.parse(sc.next(), formatterDate);
                                    LocalTime EndTime = LocalTime.parse(sc.next(), formatterTime);

                                    LocalDateTime EndDateTime = LocalDateTime.of(EndDate, EndTime);
                                    at.endOfShift(EndDateTime, employeeId);

                                } else {
                                    partEnd = true;
                                }
                            } catch (InputMismatchException e) {
                                System.out.println(ErrorWarning());
                                System.out.println(redColor + "Musis zadat cislo! (" + e + ")");
                                System.out.println(ErrorWarning());
                                System.out.println("");
                                sc.next();
                            }
                        } catch (Exception e) {
                            System.out.println(ErrorWarning());
                            System.out.println(redColor + e);
                            System.out.println(ErrorWarning());
                            System.out.println("");
                        }
                    }
                    break;
                case 4:
                    //System.out.println("Aktualne pracuje " + at.workingEmployees() + " z " + at.getEmployees().size() + " zamestnancu.");
                    System.out.println(at.workingEmployeesString());                    
                    break;
                case 5:
                    while (!partEnd) {
                        try {
                            System.out.println("Zadej ID zamestnance");
                            System.out.println("<0 = konec zadavani");
                            try {
                                int employeeId = sc.nextInt();

                                if (employeeId > 0) {
                                    System.out.println(at.displayEmployeesShifts(employeeId));

                                } else {
                                    partEnd = true;
                                }
                            } catch (InputMismatchException e) {
                                System.out.println(ErrorWarning());
                                System.out.println(redColor + "Musis zadat cislo! (" + e + ")");
                                System.out.println(ErrorWarning());
                                System.out.println("");
                                sc.next();
                            }
                        } catch (Exception e) {
                            System.out.println(ErrorWarning());
                            System.out.println(redColor + e);
                            System.out.println(ErrorWarning());
                            System.out.println("");
                        }
                    }
                    break;
                        
                case 6:
                    while (!partEnd) {
                        try {
                            System.out.println("Zadej ID zamestnance");
                            System.out.println("<0 = konec zadavani");
                            try {
                                int employeeId = sc.nextInt();

                                if (employeeId > 0) {
                                    Employee e = at.findEmployee(employeeId);

                                    ExcelWriter.write(e);

                                } else {
                                    partEnd = true;
                                }
                            } catch (InputMismatchException e) {
                                System.out.println(ErrorWarning());
                                System.out.println(redColor + "Musis zadat cislo! (" + e + ")");
                                System.out.println(ErrorWarning());
                                System.out.println("");
                                sc.next();
                            }
                        } catch (Exception e) {
                            System.out.println(ErrorWarning());
                            System.out.println(redColor + e);
                            System.out.println(ErrorWarning());
                            System.out.println("");
                        }
                        System.out.println("Export proveden uspesne.");
                    }

                    break;
                case 7:
                    System.out.println(at.displayEmployees());
                    break;
                case 8:
                    end = true;
                    break;
                default:
                    System.out.println("ZADEJTE PLATNOU VOLBU");
                    break;

            }

        }

    }

    public static String ErrorWarning() {
        return resetColor + "!!!!!!!";
    }

}
