package utils;

import app.Employee;
import app.Shift;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

/**
 *
 * @author Tomas
 */
public class ExcelWriter {
    
    public static DateTimeFormatter formatterDate = DateTimeFormatter.ofPattern("dd.MM.yyyy");
    public static DateTimeFormatter formatterTime = DateTimeFormatter.ofPattern("HH:mm");

    public static void write(Employee e) throws FileNotFoundException, IOException {
        int counter = 1;
        ArrayList<Shift> s = e.getShifts();        
        s.sort(new ComparatorByDateTime());
        try (HSSFWorkbook workbook = new HSSFWorkbook()) {
            HSSFSheet sheet = workbook.createSheet(e.getFullName());
                
                HSSFRow row00 = sheet.createRow(0);
                HSSFCell cell000 = row00.createCell(0);
                HSSFCell cell001 = row00.createCell(1);
                cell000.setCellValue(e.getFullName());
                cell001.setCellValue("Nastup = " + e.getStartDate().format(formatterDate));
            
                HSSFRow row0 = sheet.createRow(1);
                HSSFCell cell00 = row0.createCell(0);
                HSSFCell cell11 = row0.createCell(1);
                HSSFCell cell22 = row0.createCell(2);
                HSSFCell cell33 = row0.createCell(3);
                HSSFCell cell44 = row0.createCell(4);
                HSSFCell cell55 = row0.createCell(5);
                cell00.setCellValue("#");
                cell11.setCellValue("Datum prichodu");
                cell22.setCellValue("Prichod");
                cell33.setCellValue("Odchod");
                cell44.setCellValue("Datum odchodu");
                cell55.setCellValue("Delka smeny");
            
            
            
            
            for (int i = 0; i < s.size(); i++) {
                HSSFRow row = sheet.createRow(counter+1);
                HSSFCell cell0 = row.createCell(0);
                HSSFCell cell1 = row.createCell(1);
                HSSFCell cell2 = row.createCell(2);
                HSSFCell cell3 = row.createCell(3);
                HSSFCell cell4 = row.createCell(4);
                HSSFCell cell5 = row.createCell(5);
                cell0.setCellValue(counter);
                cell1.setCellValue(s.get(i).getStartDateTime().toLocalDate().format(formatterDate));
                cell2.setCellValue(s.get(i).getStartDateTime().toLocalTime().toString());
                cell3.setCellValue(s.get(i).getEndDateTime().toLocalTime().toString());
                if (!s.get(i).getStartDateTime().toLocalDate().equals(s.get(i).getEndDateTime().toLocalDate())) {
                    cell4.setCellValue(s.get(i).getEndDateTime().toLocalDate().format(formatterDate));
                }

                cell5.setCellValue(s.get(i).getShiftLength());
                counter++;
            }

            workbook.write(new FileOutputStream(e.getName()+ "_" + e.getSurname() + "_smeny.xls"));
        } catch (Exception ex) {
            System.out.println(ex);
        }
    }

    public static void main(String[] args) {

    }
}
