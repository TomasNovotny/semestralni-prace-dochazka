/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import app.Shift;
import java.time.Duration;
import java.util.Comparator;

/**
 *
 * @author Tomas
 */

public class ComparatorByDateTime implements Comparator<Shift>{ 
    
    @Override
    public int compare(Shift o1, Shift o2) {
        return (int)Duration.between(o2.getStartDateTime(), o1.getStartDateTime()).toMinutes();
    }
}
