package app;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.NoSuchElementException;
import utils.ComparatorByDateTime;

/**
 *
 * @author Tomas
 */
public class Employee implements Comparable<Employee> {

    private final String Name;
    private final String Surname;
    private final String FullName;
    private final int EmployeeId;
    private final LocalDate StartDate;
    private ArrayList<Shift> shifts;
    private boolean isWorking;

    public Employee(String Name, String Surname, int EmployeeId, LocalDate StartDate) {
        this.Name = Name;
        this.Surname = Surname;
        this.EmployeeId = EmployeeId;
        this.StartDate = StartDate;
        this.isWorking = false;
        shifts = new ArrayList<>();

        this.FullName = Name + " " + Surname;
    }
   /**
    * Prida smenu do PAMETI
    * @param s Smena
    * @return Vraci true, pokud vse problehlo v poradku. Vraci false, pokud nesedi ID zamestnance.
    */
    public boolean addShift(Shift s) {
        if (s.getEmployeeId() == this.EmployeeId) {            
            shifts.add(s);
            return true;
        }
        return false;
    }
    /**
     * Prichod zamestnance do prace. - Zmeni hodnotu isWowking na true. Pokud jiz zamestnanec pracuje vyhodi se NoSuchElementException.
     */
    public void arrival(){
        if(!isWorking){
        isWorking = true;        
        }   
        else {
            throw new NoSuchElementException("Nelze zacit smenu, pokud predchozi neskoncila!");
        }
    }
    /**
     * Ukonceni smeny zamestnance.
     * @param EndDateTime Datum a cas odchodu
     * @return Vraci smenu, ktera prave byla ukonceni. Pokud smena ani nezacala vyhodi se NoSuchElementException.
     */
    public Shift endOnGoingShift(LocalDateTime EndDateTime){
        int numberOfShifts = shifts.size();
        Shift actualShift = shifts.get(numberOfShifts-1);
        if(actualShift.getStartDateTime().isAfter(EndDateTime) || actualShift.getStartDateTime().isEqual(EndDateTime)) {
            throw new IllegalArgumentException("Cas zacatku smeny je az po konci smeny!");
        }
        if(isWorking && actualShift.getEndDateTime() == null){            
            actualShift.setEndTime(EndDateTime);
            isWorking = false;
            return actualShift;
        }
        throw new NoSuchElementException("Nelze ukoncit smenu, ktera nezacala!");
        
    }
    

    @Override
    public String toString() {
        return "Employee{" + "FullName=" + FullName + ", EmployeeId=" + EmployeeId + ", shifts=" + shifts + '}';
    }

    public boolean isIsWorking() {
        return isWorking;
    }

    public ArrayList<Shift> getShifts() {
        return shifts;
    }
    
    public String getName() {
        return Name;
    }

    public String getSurname() {
        return Surname;
    }

    public String getFullName() {
        return FullName;
    }

    public int getEmployeeId() {
        return EmployeeId;
    }

    public LocalDate getStartDate() {
        return StartDate;
    }

    @Override
    public int compareTo(Employee o) {        
        if(StartDate.isAfter(o.getStartDate())){
            return 1;
        }else if(StartDate.isBefore(o.getStartDate())){
            return -1;
        }
        return 0;
    }
    
    /**
     * Serazeni smen podle datumu od nejstarsich po nejnovejsi.
     */
    public void sortShifts(){
        Collections.sort(shifts,new ComparatorByDateTime());
    }
}
