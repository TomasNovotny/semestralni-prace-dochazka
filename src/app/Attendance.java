package app;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 *
 * @author Tomas
 */
public class Attendance {

    private ArrayList<Employee> employees;
    private ArrayList<Shift> shifts;
    private File shiftsFile;
    public DateTimeFormatter formatterDateTime = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm");
    public DateTimeFormatter formatterDate = DateTimeFormatter.ofPattern("dd.MM.yyyy");
    public DateTimeFormatter formatterTime = DateTimeFormatter.ofPattern("HH:mm");

    public Attendance() {
        employees = new ArrayList<>();
        shifts = new ArrayList<>();
    }

    public String Process(String employees, String shifts) throws FileNotFoundException, IOException {
        loadEmployees(employees);
        loadShifts(shifts);
        return "OK";
    }
/**
 * Nacita do pameti zamestnance.
 * @param file Soubor "employees.dat", z ktereho se nacitaji zamestnanci
 * @throws FileNotFoundException
 * @throws IOException 
 */
    private void loadEmployees(String file) throws FileNotFoundException, IOException {
        File employeesFile = new File(file);
        String name;
        String surname;
        int employeeId;
        LocalDate StartDate;
        int dateD, dateM, dateY;
        try (DataInputStream dataEmployees = new DataInputStream(new FileInputStream(employeesFile))) {
            boolean end = false;
            while (!end) {
                try {
                    name = dataEmployees.readUTF();
                    surname = dataEmployees.readUTF();
                    employeeId = dataEmployees.readInt();
                    dateD = dataEmployees.readInt();
                    dateM = dataEmployees.readInt();
                    dateY = dataEmployees.readInt();

                    StartDate = LocalDate.of(dateY, dateM, dateD);

                    Employee e = new Employee(name, surname, employeeId, StartDate);
                    employees.add(e);
                } catch (IOException e) {
                    end = true;
                }
            }
        }
    }
/**
 * Nacita do pameti smeny.
 * @param file Soubor "shifts.txt", z ktereho se nacitaji smeny
 * @throws FileNotFoundException
 * @throws IOException 
 */
    private void loadShifts(String file) throws FileNotFoundException, IOException {
        shiftsFile = new File(file);
        try (BufferedReader inShifts = new BufferedReader(new FileReader(shiftsFile))) {
            String line;
            String parts[];
            LocalDateTime dateTimeStart;
            LocalDateTime dateTimeEnd;
            int employeeId;
            while ((line = inShifts.readLine()) != null) {
                parts = line.split(";");
                employeeId = Integer.parseInt(parts[0]);
                dateTimeStart = LocalDateTime.parse(parts[1]);
                dateTimeEnd = LocalDateTime.parse(parts[2]);
                Shift s = new Shift(dateTimeStart, dateTimeEnd, employeeId);
                shifts.add(s);
                Employee e = findEmployee(employeeId);
                e.addShift(s);
            }
        }
    }
    /**
     * Uklada smenu do souboru.
     * @param s smena
     * @throws IOException 
     */
    private void saveShift(Shift s) throws IOException {
        try (PrintWriter outShift = new PrintWriter(new FileWriter(shiftsFile, true))) {

            outShift.print(s.getEmployeeId());
            outShift.print(";");
            outShift.print(s.getStartDateTime());
            outShift.print(";");
            outShift.print(s.getEndDateTime());
            outShift.println("");

        }
    }
    /**
     * Hleda zamestnance podle zadaneho ID.
     * @param employeeId ID zamestnance
     * @return Vraci zamestnance podle zadaneho ID. Pokud neni nalezen, je vyhozena vyjimka NoSuchElementException.
     */
    public Employee findEmployee(int employeeId) {
        for (Employee employee : employees) {
            if (employee.getEmployeeId() == employeeId) {
                return employee;
            }
        }
        throw new NoSuchElementException("Takovy zamestnanec neexistuje!");
    }
    /**
     * Prida zadanou (celou) smenu do PAMETI i SOUBORU, kde je zadan rovnou cas prichodu i odchodu.
     * @param StartDateTime Datum a cas prichodu
     * @param EndDateTime Datum a cas odchodu
     * @param EmployeeId ID zamestnance
     * @throws IOException 
     */
    public void addShiftAll(LocalDateTime StartDateTime, LocalDateTime EndDateTime, int EmployeeId) throws IOException {
        if (StartDateTime.isAfter(EndDateTime) || StartDateTime.isEqual(EndDateTime)) {
            throw new IllegalArgumentException("Cas zacatku smeny je az po konci smeny!");
        }
        Shift s = new Shift(StartDateTime, EndDateTime, EmployeeId);
        shifts.add(s);
        Employee e = findEmployee(EmployeeId);
        e.addShift(s);
        saveShift(s);
    }
    /**
     * Ulozi zadany prichod zamestnance do PAMETI. (Uzivatel zada jen prichod.)
     * @param StartDateTime Datum a cas prichodu
     * @param EmployeeId ID zamestnance
     */
    public void addShiftStart(LocalDateTime StartDateTime, int EmployeeId) {
        Shift s = new Shift(StartDateTime, EmployeeId);
        shifts.add(s);
        Employee e = findEmployee(EmployeeId);
        e.addShift(s);
        e.arrival();
    }
    /**
     * Prida do aktualni smeny zamestatnance v PAMETI cas jejiho ukonceni. (Ukonci se smena a ulozi do SOUBORU.)
     * @param EndDateTime Datum a cas odchodu
     * @param EmployeeId ID zamestnance
     * @throws IOException 
     */
    public void endOfShift(LocalDateTime EndDateTime, int EmployeeId) throws IOException {
        Employee e = findEmployee(EmployeeId);
        Shift s = e.endOnGoingShift(EndDateTime);
        if (s != null) {
            saveShift(s);
        }

    }

    /*public int workingEmployees(){
        int counter = 0;
        for (Employee employee : employees) {
           if(employee.isIsWorking() == true){
               counter++;
           }
       }
        return counter;
    }*/
    
    /**
     * 
     * @return Vraci String, kde se nachazi jmena vsech zamestnancu, kteri aktualne pracuji. 
     */
    public String workingEmployeesString() {
        int counter = 0;
        StringBuilder sb = new StringBuilder();
        sb.append(System.getProperty("line.separator"));
        for (Employee employee : employees) {
            if (employee.isIsWorking() == true) {
                sb.append("- ");
                sb.append(employee.getFullName());
                sb.append(System.getProperty("line.separator"));
                counter++;
            }
        }
        sb.append("Aktualne pracuje ").append(counter).append(" z ").append(employees.size()).append(" zamestnancu.");
        sb.append(System.getProperty("line.separator"));
        return sb.toString();
    }

    /**
     * 
     * @param employeeId ID zamestnance
     * @return Vraci na zaklade ID, strukturovany vypis vsech smen zamestnance. + Celkovy vypis hodin.
     */
    public String displayEmployeesShifts(int employeeId) {
        Employee e = findEmployee(employeeId);
        e.sortShifts();
        StringBuilder sb = new StringBuilder();
        ArrayList<Shift> sh = new ArrayList<>();
        sh = e.getShifts();
        int counter = 1;
        double hourCounter = 0;
        Shift s;
        Iterator<Shift> iterator = sh.iterator();
        sb.append(e.getFullName()).append(" (nastup: ").append(e.getStartDate().format(formatterDate)).append(")");
        sb.append(System.getProperty("line.separator"));

        while (iterator.hasNext()) {
            s = iterator.next();
            sb.append(counter).append(". - ");
            sb.append(s.getStartDateTime().format(formatterDate)).append(" ");
            sb.append(s.getStartDateTime().format(formatterTime)).append("-");
            sb.append(s.getEndDateTime().format(formatterTime));
            if (!s.getStartDateTime().toLocalDate().equals(s.getEndDateTime().toLocalDate())) {
                sb.append("(").append(s.getEndDateTime().toLocalDate().format(formatterDate)).append(")");
            }
            sb.append(System.getProperty("line.separator"));

            hourCounter = hourCounter + s.getShiftLength();
            counter++;
        }
        sb.append("Celkem: ").append(hourCounter).append(" hodin.");
        return sb.toString();
    }

    /**
     * Seradi zamestnance podle datumu, kdy zacali pracovat od nejstarsich po nejnovejsi.
     */
    public void sortByStartDate() {
        Collections.sort(employees);
    }

    /**
     * 
     * @return Vraci strukturovany vypis vsech zamestnancu.
     */
    public String displayEmployees() {
        sortByStartDate();
        StringBuilder sb = new StringBuilder();
        int counter = 1;
        Employee e;
        Iterator<Employee> iterator = employees.iterator();
        while (iterator.hasNext()) {
            e = iterator.next();
            sb.append(counter).append(". - ");
            sb.append(e.getFullName()).append(" [ID=").append(e.getEmployeeId()).append("] (");
            sb.append(e.getStartDate().format(formatterDate)).append(")");
            sb.append(System.getProperty("line.separator"));

            counter++;
        }
        return sb.toString();
    }

    /**
     * 
     * @return Vraci ArrayList zamestnancu. 
     */
    public ArrayList<Employee> getEmployees() {
        return employees;
    }

    /**
     * 
     * @return Vraci ArrayList vsech smen. 
     */
    public ArrayList<Shift> getAllShifts() {
        return shifts;
    }
    
    /*public void addEmployee(String name, String surname, LocalDate startDate) throws FileNotFoundException, IOException{
        int id = 1;
        
        for (int i = 0; i < employees.size(); i++) {            
            for (Employee employee : employees) {
                if(employee.getEmployeeId() == id){
                    id++;
            }
        }  
        }
        employees.add(new Employee(name, surname, id, startDate));
        File f = new File("employees.dat");
        try (DataOutputStream out = new DataOutputStream(new FileOutputStream(f,true))) {

            out.writeUTF(name);
            out.writeUTF(surname);
            out.writeInt(id);
            out.writeInt(startDate.getDayOfMonth());
            out.writeInt(startDate.getMonthValue());
            out.writeInt(startDate.getYear());            

        }
    }*/
    
    
    /**
     * Tesstovaci main.
     * @param args
     * @throws IOException 
     */
    public static void main(String[] args) throws IOException {
        Attendance at = new Attendance();
        at.Process("employees.dat", "shifts.txt");
        System.out.println("konec");
    }

}
