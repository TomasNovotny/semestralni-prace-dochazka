package app;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Duration;
import java.util.Comparator;

/**
 *
 * @author Tomas
 */
public class Shift{    
    private final LocalDateTime StartDateTime;
    private LocalDateTime EndDateTime;
    private final int EmployeeId;
    private double ShiftLength;    

    public Shift(LocalDateTime StartDateTime, LocalDateTime EndDateTime, int EmployeeId) {        
        this.StartDateTime = StartDateTime;
        this.EndDateTime = EndDateTime;
        this.EmployeeId = EmployeeId;
        Duration duration = Duration.between(StartDateTime, EndDateTime);
        this.ShiftLength = Math.round(duration.toMinutes()/60.0*100)/100.0;
    }
    
    public Shift(LocalDateTime StartDateTime, int EmployeeId) {        
        this.StartDateTime = StartDateTime;
        this.EmployeeId = EmployeeId;
        this.EndDateTime = null;
    }
    /**
     * Nastavi datum a cas, kdy zamestnanec opusti pracoviste. (Nastaví pokud je však hodnota null.)
     * @param EndDateTime Datum a cas odchodu
     */
    public void setEndTime(LocalDateTime EndDateTime) {
        if(this.EndDateTime == null){
            this.EndDateTime = EndDateTime;            
            Duration duration = Duration.between(StartDateTime, EndDateTime);
            this.ShiftLength = Math.round(duration.toMinutes()/60.0*100)/100.0;           
        }        
    }

    @Override
    public String toString() {
        return "Smena{ Zacatek = " + StartDateTime + ", Konec = " + EndDateTime + " (" + ShiftLength +" hod.) }";
    }
    
    

    public LocalDateTime getStartDateTime() {
        return StartDateTime;
    }

    public LocalDateTime getEndDateTime() {
        return EndDateTime;
    }

    public int getEmployeeId() {
        return EmployeeId;
    }

    public double getShiftLength() {
        return ShiftLength;
    }

    
    
    
    
    
    
    
    
    
}
